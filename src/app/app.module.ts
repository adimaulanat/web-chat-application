import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { InboxComponent } from './inbox/inbox.component';
import { ConversationComponent } from './conversation/conversation.component';
import {FormsModule} from '@angular/forms'
import { SearchFilterPipe } from './pipes/search.filter.pipe';

const appRoutes: Routes = [
  { path: 'conversation/:id_chat/:subject_id', component: ConversationComponent }
  // { path: 'conversation/:id_chat/:id', component: ConversationComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    InboxComponent,
    SearchFilterPipe,
    ConversationComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    RouterModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
