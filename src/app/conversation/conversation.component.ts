import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { chatDummy } from '../shared/user-chat';
import { Subjects } from '../shared/subject';
import { Message } from "../model/message";

@Component({
  selector: 'conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {


  private message: Message;
  public id: number;
  public subject_id: number;
  contentChat: string;
  arrChat = []
  chatUser: any = []
  subject = Subjects
  arrSubject = []
  userId: number = 1
  constructor(
    private router: Router, private route: ActivatedRoute
  ) {
    this.ngOnInit()
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.chatUser = []
      this.arrSubject = []
      this.id = params['id_chat']
      this.subject_id = params['subject_id']
      
      for (let i = 0; i < this.subject.length; i++){
        if (this.subject[i].id_chat == this.id){
          this.arrSubject.push(this.subject[i])
        }
        
        }

      for (let i = 0; i < chatDummy.length; i++) {
        if (chatDummy[i].id_chat == this.id && chatDummy[i].subject_id == this.subject_id) {
          this.chatUser.push(chatDummy[i])
        }
      }
      this.arrChat = this.chatUser
      console.log(this.arrChat)
    })
  }

  sendMessages() {
    console.log(this.chatUser[0])
    let date = new Date()
    this.message = new Message();
    this.message.id_chat = this.id
    this.message.contact_id = this.chatUser[0].contact_id
    this.message.list_subject = this.chatUser[0].list_subject
    this.message.sender = this.userId
    this.message.subject_id = this.subject_id
    this.message.time_send = date.toString();
    this.message.to = this.chatUser[0].contact_id
    this.message.messages = this.contentChat

    console.log(this.message)
    this.chatUser.push(this.message)
  }

  getSingleValue(identifier: any, source: any[], keySource: string) {
    return source.find((item) => item[keySource] == identifier);
  }

  onChangeSubject(selectedData: any, index: any){
    this.arrSubject[index]['subject_name'] = this.getSingleValue(selectedData, Subjects, "id").subject_name
  }

}
