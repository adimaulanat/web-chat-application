import { Component, OnInit } from "@angular/core";
import { user } from "../shared/dummy-user";
import { chatDummy } from "../shared/user-chat";
import { Subjects } from "../shared/subject";

@Component({
  selector: "inbox",
  templateUrl: "./inbox.component.html",
  styleUrls: ["./inbox.component.css"]
})
export class InboxComponent implements OnInit {
  userInfo = user;
  chatUser = chatDummy;
  subjects = Subjects;
  keyword = '';
  sortArr = [];
  constructor() {
    console.log(this.userInfo, this.chatUser);

    for (let i = 0; i < this.chatUser.length; i++) {
      this.onChangeName(this.chatUser[i].sender, i);
      this.onChangeSubject(this.chatUser[i].subject_id, i);
    }

    this.ngOnInit();
  }

  ngOnInit() {
    let tmp = 0;
    let isStore = false;
    for (let i = 0; i < this.chatUser.length; i++) {
      isStore = false;
      if (this.sortArr.length == 0) {
        this.sortArr.push(this.chatUser[i]);
        tmp = this.chatUser[i].id_chat;
      } else {
        for (let j = 0; j < this.sortArr.length; j++) {
          if (this.chatUser[i].id_chat != this.sortArr[j].id_chat) {
            if (j == this.sortArr.length - 1 && isStore == false) {
              this.sortArr.push(this.chatUser[i]);
              tmp = this.chatUser[i].id_chat;
            }
          } else {
            isStore = true;
          }
        }
      }
    }
    console.log(this.sortArr);
  }

  getSingleValue(identifier: any, source: any[], keySource: string) {
    return source.find(item => item[keySource] == identifier);
  }

  onChangeName(selectedData: any, index: any) {
    this.chatUser[index]["sender_name"] = this.getSingleValue(
      selectedData,
      this.userInfo,
      "id"
    ).name;
    this.chatUser[index]["list_subject"] = this.getSingleValue(
      selectedData,
      this.userInfo,
      "id"
    ).list_subject;
  }

  onChangeSubject(selectedData: any, index: any) {
    this.chatUser[index]["subject_name"] = this.getSingleValue(
      selectedData,
      this.subjects,
      "id"
    ).subject_name;
  }

  activateClass(chat_list) {
    chat_list.active = !chat_list.active
  }
    
}
