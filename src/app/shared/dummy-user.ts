export const user = [
    {
        "id" : 13,
        "name" : "Adi Maulana Triadi",
        "list_subject" : [1, 2, 3],
        "photo" : ""
    },
    {
        "id" : 1,
        "name" : "Maulana Triadi",
        "list_subject" : [1, 2],
        "photo" : ""
    },
    {
        "id" : 2,
        "name" : "Nabila Gardenia",
        "list_subject" : [3, 4, 5],
        "photo" : ""
    },
    {
        "id" : 3,
        "name" : "Usep Munandar",
        "list_subject" : [1, 2, 3],
        "photo" : ""
    },
    {
        "id" : 4,
        "name" : "Gardenia",
        "list_subject" : [1, 2, 3],
        "photo" : ""
    },
    {
        "id" : 5,
        "name" : "Natasha",
        "list_subject" : [1, 2, 3],
        "photo" : ""
    }
]