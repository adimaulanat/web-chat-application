export const chatDummy = [
    {
        "id_chat" : 345, 
        "sender" : 1,
        "to" : 2,
        "contact_id": 2,
        "subject_id": 1,
        "messages" : "Computer users and programmers have become so accustomed to using Windows, even for the changing capabilities and the appearances of the graphical interface of the versions, therefore it has remained Microsoft’s product. Although, Lycoris, Red Hat, Mandrake, Suse, Knoppix, Slackware and Lindows make up some of the different versions of LINUX. These companies release their own versions of the operating systems with minor changes, and yet always with the same bottom line. The simple fact that not one of these companies are close to competing with Windows, for the most part causes the difference in market share.",
        "time_send" : "00.39"
    },
    {
        "id_chat" : 345, 
        "sender" : 2,
        "to" : 1,
        "contact_id": 2,
        "subject_id": 1,
        "messages" : "It sometimes seems everyone on the planet is using Windows. Many say Windows is way better than LINUX because of the simple handling of the software or hardware. There are huge differences between the number of users of LINUX and Windows. Many say LINUX is far better because it started as an Open Source software and thus is more flexible than Windows. Then what accounts for the enormous difference in market share between these two operating systems?",
        "time_send" : "00.45"
    },
    {
        "id_chat" : 345, 
        "sender" : 1,
        "to" : 2,
        "contact_id": 2,
        "subject_id": 1,
        "messages" : "Makasih ya",
        "time_send" : "00.54"
    },
    {
        "id_chat" : 123, 
        "sender" : 2,
        "to" : 1,
        "contact_id": 2,
        "subject_id": 3,
        "messages" : "Makasih ya",
        "time_send" : "00.54"
    },
    {
        "id_chat" : 345, 
        "sender" : 2,
        "to" : 1,
        "contact_id": 2,
        "subject_id": 1,
        "messages" : "Makasih ya",
        "time_send" : "00.54"
    },
    {
        "id_chat" : 467, 
        "sender" : 3,
        "to" : 1,
        "contact_id": 3,
        "subject_id": 2,
        "messages" : "Hayu ikut",
        "time_send" : "00.54"
    },
    //Chat for other subject
    {
        "id_chat" : 345, 
        "sender" : 1,
        "to" : 2,
        "contact_id": 2,
        "subject_id": 4,
        "messages" : "Computer users and programmers have become so accustomed to using Windows, even for the changing capabilities and the appearances of the graphical interface of the versions, therefore it has remained Microsoft’s product. Although, Lycoris, Red Hat, Mandrake, Suse, Knoppix, Slackware and Lindows make up some of the different versions of LINUX. These companies release their own versions of the operating systems with minor changes, and yet always with the same bottom line. The simple fact that not one of these companies are close to competing with Windows, for the most part causes the difference in market share.",
        "time_send" : "00.39"
    },
    {
        "id_chat" : 345, 
        "sender" : 2,
        "to" : 1,
        "contact_id": 2,
        "subject_id": 4,
        "messages" : "It sometimes seems everyone on the planet is using Windows. Many say Windows is way better than LINUX because of the simple handling of the software or hardware. There are huge differences between the number of users of LINUX and Windows. Many say LINUX is far better because it started as an Open Source software and thus is more flexible than Windows. Then what accounts for the enormous difference in market share between these two operating systems?",
        "time_send" : "00.45"
    },
    {
        "id_chat" : 345, 
        "sender" : 2,
        "to" : 1,
        "contact_id": 2,
        "subject_id": 5,
        "messages" : "Makasih ya",
        "time_send" : "00.54"
    },
    {
        "id_chat" : 345, 
        "sender" : 2,
        "to" : 1,
        "contact_id": 2,
        "subject_id": 6,
        "messages" : "Mana tugas aku ya",
        "time_send" : "00.54"
    },
]